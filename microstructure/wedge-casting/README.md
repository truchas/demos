# Microstructure Analysis Demo Problem

This demo illustrates the microstructure analysis capability of Truchas. The
problem simulates the solidification of a Sn-Bi alloy in a small wedge casting
designed to produce a wide variety of solidification conditions and range of
microstructural characteristics.

The 3D quarter symmetry computational model includes the graphite mold that
contains the cast Sn-Bi wedge. HTC BC are imposed on the external mold surface
and internal HTC BC are imposed on the interface between the mold and casting.
The mold is initially cool with a vertical temperature gradient, and the alloy
is initially liquid at the hot pour temperature.

This demo is derived from full simulations of wedge casting validation
experiments; see for example, Gibbs, et al, "Embedding a microstructure model
in a macro-scale solidification model", LA-UR-16-20851, and "Embedding a
microstructural model into a continuum-scale casting code", LA-UR-16-27631.

